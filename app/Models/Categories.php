<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'categories';
    protected $fillable = ['name', 'slug'];

    /**
     * Get the Articles for the blog Category.
     */
    public function articles()
    {
        return $this->hasMany(Articles::class);
    }
}
