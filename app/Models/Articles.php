<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    use HasFactory;

    protected $table = 'articles';
    protected $fillable = ['title', 'slug', 'content', 'category_id', 'media'];

    public function Categories()
    {
        return $this->belongsTo(Categories::class);
    }

}
