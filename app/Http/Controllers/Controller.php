<?php

namespace App\Http\Controllers;
use App\Models\Categories;
use Encore\Admin\Layout\Content;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function all(){
        return response()->json(Categories::all());
    }

    public function sort(Request $request, $id){

        $categories = Categories::find($id);
        $articles = $categories->articles;

        return response()->json($articles);
    }
}
