<?php

namespace Database\Seeders;
use App\Models\Categories;
use App\Models\Articles;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++){
            Categories::insert([
                'name' => 'cat_'.$i,
                'slug' => 'cat'.$i,
            ]);
            for ($k = 1; $k <= 20; $k++){
                Articles::insert([
                    'title' => 'title_'.$i.'_'.$k,
                    'slug' => 'articles_'.$i.'_'.$k,
                    'content' => 'content text text text text text text text'.$i.'-'.$k,
                    'categories_id' => $i,
                    'media' => 'media-'.$i.'-'.$k,
                ]);
            }
        }

    }
}
