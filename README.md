## Install Laravel

- composer i

- npm i

- php artisan migrate

- php artisan db:seed Automatic database filling

## Additional commands

- composer require laravel/ui

- php artisan ui vue

- php artisan ui vue --auth

- npm install 

- npm run dev

- npm run watch